# Single-Image-Stereogram embedded in a 3D scene 
[![Build status](https://gitlab.com/juliendehos/spation/badges/master/build.svg)](https://gitlab.com/juliendehos/spation/pipelines) 

This program displays a single-image-stereogram (SIS) into a 3D scene. Thus, 
the final displayed image contains both the main 3D scene and the included SIS 
scene. The viewer can see the main scene, when focusing his eyes on the display
screen, or the SIS scene, when focusing behind. The method is implemented on 
the GPU and produces Full-HD images in real-time. 

## Build & run
install opengl, glu, freeglut, glm, glew, libpng
```
make
cd bin
./spation
```

* * * *

# Image multi-scène par intégration d’un autostéréogramme dans une scène 3D 

## Papier

Julien Dehos, Pierre-Alexandre Hébert, Christophe Renaud; <br>
Image multi-scène par intégration d’un autostéréogramme dans une scène 3D; <br>
Revue Électronique Francophone d’Informatique Graphique, Volume 8, Numéro 2, pp. 67–78, 2014  <br>

[\[hal-01081800\]](https://hal.archives-ouvertes.fr/hal-01081800)

##  Résumé

Dans ce papier, nous proposons d’intégrer un autostéréogramme dans une scène 3D
de telle sorte que l’image finalement affichée contienne à la fois la scène 3D
principale et la scène de l’autostéréogramme. En focalisant son regard sur
l’écran d’affichage ou derrière celui-ci, l’utilisateur peut alors voir l’une
ou l’autre des deux scènes, réalisant ainsi une sorte de "contrepèterie
visuelle". La méthode s’implémente en quelques passes de rendu sur GPU, et
permet d’obtenir un affichage Full-HD en temps-réel.

## Vidéo

[![demo on vimeo](paper/image_spation.jpg)](https://vimeo.com/154505124)

## Exemple 1

- Autostéréogramme dans la scène 3D :

![](paper/tv_scene.png)

- Détail de l'autostéréogramme :

![](paper/tv_sis.png)

- Image de profondeur :

![](paper/tv_sis_depth.png)

## Exemple 2

- Autostéréogramme dans la scène 3D :

![](paper/dragons_scene.png)

- Détail de l'autostéréogramme :

![](paper/dragons_sis.png)

- Image de profondeur :

![](paper/dragons_sis_depth.png)

