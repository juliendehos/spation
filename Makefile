# Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

BIN = ./bin/spation
PACKAGES = glew libpng 
LIBS = -lglut
CXXFLAGS = -O2 -Wall -Wextra -std=c++11 -fPIC

# ubuntu : you have this error : Inconsistency detected by ld.so: ...
# then uncomment the line below (and check driver version)
#LDFLAGS += -L/usr/lib/nvidia-304/

CXXFLAGS += `pkg-config --cflags $(PACKAGES)`
LDFLAGS +=`pkg-config --libs-only-L --libs-only-other $(PACKAGES)`
LIBS +=`pkg-config --libs-only-l $(PACKAGES)`

SRC = $(shell find ./src/ -name *.cpp) 
OBJ = $(SRC:.cpp=.o)

.PHONY : all clean

all: $(BIN)

$(BIN) : $(OBJ)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	find . -name "*.o" -delete
