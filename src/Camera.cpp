// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Camera.hpp"

Camera::Camera() {
    updateView(0, 0, 0, 0, 0, 0);
}

void Camera::updateView(float tx, float ty, float tz,
        float rx, float ry, float rz) {

    _rx = rx;
    _ry = ry;
    _rz = rz;

    _tx += tx*cosf(_ry) + tz*sinf(_ry);
    _ty += ty;
    _tz += tz*cosf(_ry) - tx*sinf(_ry);
}

glm::mat4 Camera::getOrientationMatrix() {
    glm::mat4 Rx = glm::rotate(glm::mat4(1.f), -_rx, glm::vec3(1.f, 0.f, 0.f));
    glm::mat4 Ry = glm::rotate(glm::mat4(1.f), -_ry, glm::vec3(0.f, 1.f, 0.f));
    return Rx * Ry;
}

glm::mat4 Camera::getTranslationMatrix() const {
    return glm::translate(glm::mat4(1.f), glm::vec3(-_tx, -_ty, -_tz));
}

glm::vec3 Camera::getTranslationVector() const {
    return glm::vec3(_tx, _ty, _tz);
}
