// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Texture.hpp"
#include "Util.hpp"

#include <png.h>

void Texture::bind() const {
    glBindTexture(GL_TEXTURE_2D, _textureId);
}

void Texture::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}

bool Texture::initFromFile(const std::string & filename) {

    png_byte header[8];

    FILE *fp = fopen(filename.c_str(), "rb");
    if (fp == 0) {
        utilInfo("unable to open ", filename.c_str(), __FILE__, __LINE__);
        return false;
    }

    // read the header
    size_t s = fread(header, 1, 8, fp);

    if (s != 8) {
        utilInfo("cannot read header of ", filename.c_str(),
                __FILE__, __LINE__);
        fclose(fp);
        return false;
    }

    if (png_sig_cmp(header, 0, 8)) {
        utilInfo(filename.c_str(), " is not a PNG file", __FILE__, __LINE__);
        fclose(fp);
        return false;
    }

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
            NULL, NULL, NULL);
    if (!png_ptr) {
        utilInfo("png_create_read_struct returned 0", __FILE__, __LINE__);
        fclose(fp);
        return false;
    }

    // create png info struct
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        utilInfo("png_create_info_struct returned 0", __FILE__, __LINE__);
        png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        fclose(fp);
        return false;
    }

    // create png info struct
    png_infop end_info = png_create_info_struct(png_ptr);
    if (!end_info) {
        utilInfo("png_create_info_struct returned 0", __FILE__, __LINE__);
        png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp) NULL);
        fclose(fp);
        return false;
    }

    // the code in this if statement gets called if libpng encounters an error
    if (setjmp(png_jmpbuf(png_ptr))) {
        utilInfo("error from libpng", __FILE__, __LINE__);
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return false;
    }

    // init png reading
    png_init_io(png_ptr, fp);

    // let libpng know you already read the first 8 bytes
    png_set_sig_bytes(png_ptr, 8);

    // read all the info up to the image data
    png_read_info(png_ptr, info_ptr);

    // variables to pass to get info
    int bit_depth, color_type;
    png_uint_32 temp_width, temp_height;

    // get info about png
    png_get_IHDR(png_ptr, info_ptr, &temp_width, &temp_height, &bit_depth,
            &color_type, NULL, NULL, NULL);

    if (bit_depth != 8) {
        utilInfo("Unsupported bit depth. Must be 8", __FILE__, __LINE__);
        return false;
    }

    GLint format;
    switch(color_type) {
        case PNG_COLOR_TYPE_RGB:
            format = GL_RGB;
            break;
        case PNG_COLOR_TYPE_RGB_ALPHA:
            format = GL_RGBA;
            break;
        default:
            utilInfo("Unknown libpng color type.", __FILE__, __LINE__);
            return false;
    }

    // Update the png info struct.
    png_read_update_info(png_ptr, info_ptr);

    // Row size in bytes.
    int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

    // glTexImage2d requires rows to be 4-byte aligned
    rowbytes += 3 - ((rowbytes-1) % 4);

    // Allocate the image_data as a big block, to be given to opengl
    png_byte * image_data = (png_byte *)malloc(rowbytes * temp_height
            * sizeof(png_byte)+15);
    if (image_data == NULL) {
        utilInfo("could not allocate memory for PNG image data",
                __FILE__, __LINE__);
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        fclose(fp);
        return false;
    }

    // row_pointers is for pointing to image_data for reading the png
    // with libpng
    png_byte ** row_pointers =
        (png_byte **)malloc(temp_height * sizeof(png_byte *));
    if (row_pointers == NULL) {
        utilInfo("could not allocate memory for PNG row pointers",
                __FILE__, __LINE__);
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        free(image_data);
        fclose(fp);
        return false;
    }

    // set the individual row_pointers to point at the correct offsets
    // of image_data
    for (unsigned int i = 0; i < temp_height; i++) {
        row_pointers[temp_height - 1 - i] = image_data + i * rowbytes;
    }

    // read the png into image_data through row_pointers
    png_read_image(png_ptr, row_pointers);

    // Generate the OpenGL texture object
    glGenTextures(1, &_textureId);
    glBindTexture(GL_TEXTURE_2D, _textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, format, temp_width, temp_height,
            0, format, GL_UNSIGNED_BYTE, image_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
            GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    // clean up
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
    free(image_data);
    free(row_pointers);
    fclose(fp);

    return true;
}

/*
#include <SDL_image.h>
bool Texture::initFromFile(const std::string & filename) {

	// load image file
	SDL_Surface * surface = IMG_Load(filename.c_str());
	if (not surface) return false;
	GLenum textureFormat;

	switch (surface->format->BytesPerPixel) {
	case 4:
		textureFormat =
			surface->format->Rmask == 0x000000ff ? GL_RGBA : GL_BGRA;
		break;
	case 3:
		textureFormat = surface->format->Rmask == 0x000000ff ? GL_RGB : GL_BGR;
		break;
	default:
		return false;
	}

	// load texture to GPU
	glGenTextures(1, &_textureId);
	glBindTexture(GL_TEXTURE_2D, _textureId);
	glTexImage2D( GL_TEXTURE_2D, 0, surface->format->BytesPerPixel,
			surface->w, surface->h, 0,
			textureFormat, GL_UNSIGNED_BYTE, surface->pixels );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
    	GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	SDL_FreeSurface( surface );
	return true;
}
*/
