// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef UTIL_HPP_
#define UTIL_HPP_

#include <iostream>
#include <map>
#include <memory>
#include <vector>

template<typename T> using VectorUptr = std::vector< std::unique_ptr<T> >;

template<typename T> using MapUptr= std::map< std::string,std::unique_ptr<T> >;

/// \brief print an error message (+source code file/line) and exit
///
/// Typical call : utilError("my message", __FILE__, __LINE__);
void utilError(const char * message, const char * file, int line);

/// \brief print an error message (+source code file/line) and exit
///
/// Typical call : utilError("my message", __FILE__, __LINE__);
void utilError(const char * message1, const char * message2,
		const char * file, int line);

/// \brief print a message (+source code file/line)
///
/// Typical call : utilInfo("my message", __FILE__, __LINE__);
void utilInfo(const char * message, const char * file, int line);

/// \brief print a message (+source code file/line)
///
/// Typical call : utilInfo("my message", __FILE__, __LINE__);
void utilInfo(const char * message1, const char * message2,
		const char * file, int line);

#endif
