// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef MESH_HPP_
#define MESH_HPP_

#include <string>
#include <vector>

#include "Ogl.hpp"

/// \brief triangle mesh
class Mesh {
    private:
        class VertexBuffer {
            private:
                GLuint _vertexBufferId;
                GLuint _index;
                GLint _nbValuesPerVertex;
            public:
                VertexBuffer(GLuint index, GLsizei nbVertices,
                        GLint nbValuesPerVertex, GLfloat * data);
                void enable() const;
                void disable() const;
        };

    protected:
        GLenum _meshMode;
        GLuint _vertexArrayId;
        std::vector<VertexBuffer> _vertexBuffers;
        GLsizei _nbVertices;

    public:
        /// \brief initialize a mesh
        ///
        /// Call this method before pushVertexBuffer/draw.
        void initMesh(GLenum meshMode, GLsizei nbVertices);

        /// \brief add a vertex buffer in the mesh (position, normal...)
        ///
        /// Call initMesh first
        void pushVertexBuffer(GLint nbValuesPerVertex, GLfloat * data);

        /// \brief draw the mesh
        ///
        /// Call initMesh and pushVertexBuffer first
        virtual void draw() const;
};

/// \brief ElementMesh
/// \brief triangle mesh with indirect in
class ElementMesh : public Mesh {
    private:
        GLuint _elementBufferId;
        GLsizei _nbElements;

    public :
        /// \brief initialize indices
        void initElementBuffer(GLsizei nbElements, GLuint * data);

        /// \brief draw the mesh
        ///
        /// Call initMesh, initElementBuffer and pushVertexBuffer first
        virtual void draw() const;
};

#endif
