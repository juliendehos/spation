// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef SPLINE_HPP_
#define SPLINE_HPP_

#include <vector>

/// \brief abstract class for spline interpolation
template <typename T>
class Spline {
    protected:
        std::vector<double> _t;
        std::vector<T> _P;

    public:
        /// \brief add a key (time, value) to the spline
        void addKey(double t, const T & P) {
            assert(_t.empty() or t >= _t.back());
            _t.push_back(t);
            _P.push_back(P);
        }

        /// \brief get interpolated value at time t
        ///
        /// \pre t > getStartTime() and t < getEndTime()
        virtual T getValue(double t) const = 0;

        /// \brief get start time
        virtual double getStartTime() const = 0;

        /// \brief get end time
        virtual double getEndTime() const = 0;
};

/// \brief linear interpolation
template <typename T>
class Spline1 : public Spline<T> {
    public:
        T getValue(double t) const {
            assert(t >= getStartTime());
            assert(t < getEndTime());
            int iLater = 0;
            while (iLater < this->_t.size() and this->_t[iLater] <= t) iLater++;
            if (iLater == 0) return this->_P.front();
            if (iLater == this->_t.size()) return this->_P.back();
            double tu = (t - this->_t[iLater-1]) / (this->_t[iLater] 
                    - this->_t[iLater-1]);
            return (1-tu)*this->_P[iLater-1] + tu*this->_P[iLater];
        }

        double getStartTime() const {
            assert(not this->_t.empty());
            return this->_t.front();
        }

        double getEndTime() const {
            assert(not this->_t.empty());
            return this->_t.back();
        }
};

/// \brief cubic spline interpolation
template <typename T>
class Spline3 : public Spline<T> {
    public:
        T getValue(double t) const {
            assert(t >= getStartTime());
            assert(t < getEndTime());

            // find i
            int i=2;
            while (this->_t[i] <= t) i++;

            // compute points and tangents
            const double G = (1+sqrt(5))/2;
            T P1 = this->_P[i-1];
            T P4 = this->_P[i];
            T R1 = G / (this->_t[i]-this->_t[i-2])*(this->_P[i]-this->_P[i-2]);
            T R4 = G / (this->_t[i+1]-this->_t[i-1])*(this->_P[i+1]-this->_P[i-1]);

            // compute hermite interpolation
            double tu = (t-this->_t[i-1]) / (this->_t[i]-this->_t[i-1]);
            double tu2 = tu*tu;
            double tu3 = tu2*tu;
            return (2*tu3 - 3*tu2 + 1) * P1
                + (-2*tu3 + 3*tu2) * P4
                + (tu3 - 2*tu2 + tu) * R1
                + (tu3 - tu2) * R4;
        }

        double getStartTime() const {
            assert(this->_t.size() > 3);
            return this->_t[1];
        }

        double getEndTime() const {
            assert(this->_t.size() > 3);
            return this->_t[this->_t.size()-2];
        }
};

#endif
