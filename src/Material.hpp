// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef MATERIAL_HPP_
#define MATERIAL_HPP_

#include "Texture.hpp"

/// \brief Phong material
struct Material {

	/// \brief ambient coefficient
	glm::vec3 _ka;

	/// \brief diffuse coefficient
	glm::vec3 _kd;

	/// \brief specular coefficient
	glm::vec3 _ks;

	/// \brief specular index
	float _ns;

	/// \brief texture pointer
	Texture * _ptrTexture;

	/// \brief construct a default material
	Material();
};


#endif
