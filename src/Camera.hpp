// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include "Ogl.hpp"

/// \brief implement a walkthrough Camera
class Camera {
    private:
        float _tx, _ty, _tz;
        float _rx, _ry, _rz;

    public:
        /// \brief implement a walkthrough Camera
        Camera();

        /// \brief move camera (rotation in rad)
        void updateView(float tx, float ty, float tz,
                float rx, float ry, float rz);

        /// \brief get camera orientation (mat4)
        glm::mat4 getOrientationMatrix();

        /// \brief get camera translation (mat4)
        glm::mat4 getTranslationMatrix() const;

        /// \brief get camera translation (vec3)
        glm::vec3 getTranslationVector() const;
};

#endif
