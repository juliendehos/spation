// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef FRAMEBUFFER_HPP_
#define FRAMEBUFFER_HPP_

#include "Ogl.hpp"

/// \brief FrameBuffer for render to texture
class FrameBuffer {
    private:
        int _width;
        int _height;

        GLuint _frameBufferId;
        GLuint _textureId;
        GLuint _renderBufferId;

    public:
        /// \brief init FrameBuffer with texture size
        void init(int width, int height);

        /// \brief clear texture with the given color
        void clear(float r, float g, float b, float a);

        /// \brief bind buffer (begin render to texture)
        void bindFrameBuffer() const;

        /// \brief unbind buffer (end render to texture)
        static void unbindFrameBuffer();

        /// \brief bind texture (framebuffer rendering)
        void bindTexture() const;

        /// \brief unbind texture
        static void unbindTexture();

        /// \brief get texture width of the framebuffer
        int getWidth() const;

        /// \brief get texture height of the framebuffer
        int getHeight() const;
};

#endif
