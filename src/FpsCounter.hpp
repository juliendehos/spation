// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef FPSCOUNTER_HPP_
#define FPSCOUNTER_HPP_

#include <chrono>

/// \brief measure frame rate
class FpsCounter {
    private:
        std::chrono::time_point<std::chrono::system_clock> _t0;
        int _nbFrames;

    public:
        /// \brief start counter
        void start();

        /// \brief count one more frame
        void countNewFrame();

        /// \brief has a fps value available (1 s min)
        bool hasFps() const;

        /// \brief get fps value and restart counter
        float getFpsAndRestart();
};

#endif
