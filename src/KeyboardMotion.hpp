// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef KEYBOARDMOTION_HPP_
#define KEYBOARDMOTION_HPP_

#include <chrono>

/// \brief control a variable with a key
class KeyboardMotion {
    private:
        float _value;
        float _speed;
        bool _isMotion;
        std::chrono::time_point<std::chrono::system_clock> _t0;

    public:
        /// \brief constructor
        KeyboardMotion(float speed);

        /// \brief press key : start updating values
        void press();

        /// \brief release button : stop updating values
        void release();

        /// \brief get RELATIVE value (since last call)
        float getValueAndUpdate();

    private:
        void update();
};

#endif
