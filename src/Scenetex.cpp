// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "LoaderOBJ.hpp"
#include "Scenetex.hpp"

#include <cmath>
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Scenetex
//////////////////////////////////////////////////////////////////////

void Scenetex::init(int texWidth, int texHeight,
        const std::string & vsFilename, const std::string & fsFilename) {

    _fbo.init(texWidth, texHeight);

    _shader.initFromFile(vsFilename, fsFilename);

    _quadMesh.initMesh(GL_TRIANGLE_STRIP, 4);
    GLfloat vertexBufferData[] = {
        1.f,  -1.f,   0.f,
        1.f,   1.f,   0.f,
        -1.f,  -1.f,   0.f,
        -1.f,   1.f,   0.f
    };
    _quadMesh.pushVertexBuffer(3, vertexBufferData);
    GLfloat normalBufferData[] = {
        0.f, 0.f, 1.f,
        0.f, 0.f, 1.f,
        0.f, 0.f, 1.f,
        0.f, 0.f, 1.f
    };
    _quadMesh.pushVertexBuffer(3, normalBufferData);
    GLfloat uvBufferData[] = {
        1.0f,    0.0f,
        1.0f,    1.0f,
        0.0f,    0.0f,
        0.0f,    1.0f
    };
    _quadMesh.pushVertexBuffer(2, uvBufferData);
}

void Scenetex::bindTexture() const {
    _fbo.bindTexture();
}

//////////////////////////////////////////////////////////////////////
// ScenetexDepth
//////////////////////////////////////////////////////////////////////

void ScenetexDepth::init(int texWidth, int texHeight,
        const std::string & sisSceneFilename,
        const glm::mat4 & sisSceneMatrix) {

    Scenetex::init(texWidth, texHeight,
            "vsScenetexDepth.glsl", "fsScenetexDepth.glsl");

    LoaderOBJ loader;
    loader.loadOBJ(sisSceneFilename, sisSceneMatrix, _uptrObjects,
            _uptrMeshes, _uptrTextures, _uptrMaterials);
}


void ScenetexDepth::render(const glm::mat4 &T) {

    int width = _fbo.getWidth();
    int height = _fbo.getHeight();

    // render sis scene to texture
    _fbo.bindFrameBuffer();
    _shader.bind();

    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, width, height);

    glm::mat4 V = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.f, -7.f));
    glm::mat4 P = glm::perspective(45.f, width/float(height), 0.1f, 20.f);

    for (auto & ptrObject : _uptrObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _shader.uniformMatrix4fv("_M", &M[0][0]);

        glm::mat4 MVP = P * V* T * M ;

        _shader.uniformMatrix4fv("_MVP", &MVP[0][0]);

        ptrObject->getPtrMesh()->draw();
    }

    _shader.unbind();
    _fbo.unbindFrameBuffer();
}

//////////////////////////////////////////////////////////////////////
// ScenetexMap
//////////////////////////////////////////////////////////////////////

void ScenetexMap::init(int texWidth, int texHeight,
        const std::string & imageFilename) {

    Scenetex::init(texWidth, texHeight,
            "vsScenetexMap.glsl", "fsScenetexMapRandom.glsl");

    _textureImage.initFromFile(imageFilename);
    _shaderImage.initFromFile("vsScenetexMap.glsl", "fsScenetexMapImage.glsl");
}

void ScenetexMap::render(const SisConfig & config) {

    // render scene to texture
    _fbo.bindFrameBuffer();

    // if map = random dots
    if (config._mapSlider.getValue() < 2) {
        _shader.bind();
        _shader.uniform1i("_resolution", config._resolutionSlider.getValue());
        if (config._mapSlider.getValue() == 0)
            _shader.uniform1f("_time", _rng.generate());
        else 
            _shader.uniform1f("_time", 0.f);
    }
    else {
        _shaderImage.bind();
        _shaderImage.uniform1f("_repetitions",
                config._resolutionSlider.getValue()/100.f);
        _textureImage.bind();
    }	

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, _fbo.getWidth(), _fbo.getHeight());

    _quadMesh.draw();

    if (config._mapSlider.getValue() < 2) {
        _shader.unbind();
    }
    else {
        _textureImage.unbind();
        _shaderImage.unbind();
    }

    _fbo.unbindFrameBuffer();
}

//////////////////////////////////////////////////////////////////////
// ScenetexScreen
//////////////////////////////////////////////////////////////////////

void ScenetexScreen::init(int texWidth, int texHeight,
        Scenetex * ptrScenetex, VectorUptr<Object> * ptrUptrSisObjects) {

    Scenetex::init(texWidth, texHeight,
            "vsScenetexScreen.glsl", "fsScenetexScreen.glsl");

    _ptrScenetex = ptrScenetex;
    _ptrUptrSisObjects = ptrUptrSisObjects;
}

void ScenetexScreen::render(int frameWidth, int frameHeight,
        const glm::mat4 &VP, const SisConfig & /*config*/) {

    // render scene to texture
    _fbo.bindFrameBuffer();
    _shader.bind();

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, frameWidth, frameHeight);

    _ptrScenetex->bindTexture();

    for (auto & ptrObject : * _ptrUptrSisObjects) {

        glm::mat4 M = ptrObject->getModelMatrix();
        glm::mat4 MVP = VP * M;
        _shader.uniformMatrix4fv("_MVP", &MVP[0][0]);
        ptrObject->getPtrMesh()->draw();
    }

    Texture::unbind();

    _shader.unbind();
    _fbo.unbindFrameBuffer();
}

//////////////////////////////////////////////////////////////////////
// ScenetexSis
//////////////////////////////////////////////////////////////////////

void ScenetexSis::init(int texWidth, int texHeight,
        VectorUptr<Object> * ptrUptrSisObjects,
        int depthWidth, int /*depthHeight*/,
        const std::string & sisSceneFilename, const glm::mat4 & sisSceneMatrix,
        int mapWidth, int mapHeight,
        const std::string & imageMapFilename) {

    Scenetex::init(texWidth, texHeight,
            "vsScenetexSis.glsl", "fsScenetexSis.glsl");

    _scenetexDepth.init(depthWidth, depthWidth,
            sisSceneFilename, sisSceneMatrix);
    _scenetexMap.init(mapWidth, mapHeight, imageMapFilename);
    _scenetexScreenDepth.init(texWidth, texHeight,
            &_scenetexDepth, ptrUptrSisObjects);
    _scenetexScreenMap.init(texWidth, texHeight,
            &_scenetexMap, ptrUptrSisObjects);

    _fbo2.init(texWidth, texHeight);
}

void ScenetexSis::render(int frameWidth, int frameHeight, const glm::mat4 &T,
        const glm::mat4 &VP, const SisConfig & config) {

    ////////////////////////////////////////////////////////////////
    // render depth and map 
    ////////////////////////////////////////////////////////////////
    _scenetexDepth.render(T);
    _scenetexMap.render(config);
    _scenetexScreenDepth.render(frameWidth, frameHeight, VP, config);
    _scenetexScreenMap.render(frameWidth, frameHeight, VP, config);

    ////////////////////////////////////////////////////////////////
    // render sis
    ////////////////////////////////////////////////////////////////

    // global initialisations
    _fbo.clear(0.f, 0.f, 0.f, 0.f);
    _ptrFboSrc = &_fbo;
    _ptrFboDst = &_fbo2;
    _shader.bind();
    _shader.uniform2f("_frameSize", frameWidth, frameHeight);
    _shader.uniform2f("_textureSize", _fbo.getWidth(), _fbo.getHeight());
    _shader.uniform1i("_textureScenetex", 0);
    _shader.uniform1i("_textureDepthScreen", 1);
    _shader.uniform1i("_textureMapScreen", 2);
    glActiveTexture(GL_TEXTURE1);
    _scenetexScreenDepth.bindTexture();
    glActiveTexture(GL_TEXTURE2);
    _scenetexScreenMap.bindTexture();

    // render each sirds strip in a pass
    int stripWidth = config._stripSlider.getValue();
    int depthFactor = config._depthSlider.getValue();
    _shader.uniform1i("_stripWidth", stripWidth);
    _shader.uniform1i("_depthFactor", depthFactor);
    int deltaX = stripWidth - depthFactor;
    int x1 = stripWidth-1;
    if (stripWidth <= depthFactor) {
        x1 = frameWidth;
        deltaX = frameWidth;
    }
    for (int x0=0; x0<frameWidth; x0+=deltaX, x1+=deltaX) {

        _shader.uniform1i("_x0", x0);
        _shader.uniform1i("_x1", x1);

        _ptrFboDst->bindFrameBuffer();
        glActiveTexture(GL_TEXTURE0);
        _ptrFboSrc->bindTexture();

        // clear screen
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, frameWidth, frameHeight);

        _quadMesh.draw();

        glActiveTexture(GL_TEXTURE0);
        _ptrFboSrc->unbindTexture();
        _ptrFboDst->unbindFrameBuffer();

        std::swap(_ptrFboSrc, _ptrFboDst);
    }

    // global clean up
    glActiveTexture(GL_TEXTURE1);
    Texture::unbind();
    glActiveTexture(GL_TEXTURE2);
    Texture::unbind();
    glActiveTexture(GL_TEXTURE0);
    _shader.unbind();
}

void ScenetexSis::bindTextureFbo() {
    _ptrFboSrc->bindTexture();		
}

void ScenetexSis::bindTextureDepth() {
    _scenetexScreenDepth.bindTexture();
}

int ScenetexSis::getFboWidth() {
    return _ptrFboSrc->getWidth();
}

int ScenetexSis::getFboHeight() {
    return _ptrFboSrc->getHeight();
}

