
#ifndef SCENETEX_HPP_
#define SCENETEX_HPP_

#include "SisConfig.hpp"
#include "FrameBuffer.hpp"
#include "Material.hpp"
#include "Mesh.hpp"
#include "Object.hpp"
#include "Random.hpp"
#include "Shader.hpp"
#include "Texture.hpp"
#include "Util.hpp"

/// \brief main class for rendering a scene in a texture
/// (using a FrameBuffer)
class Scenetex {

protected:
	FrameBuffer _fbo;
	Shader _shader;
	Mesh _quadMesh;

public:
	/// \brief bind rendered texture
	void bindTexture() const;

	/// \brief init output texture and rendering shader
	void init(int texWidth, int texHeight, const std::string & vsFilename,
			const std::string & fsFilename);
};

/// \brief render the depth of a Scene in a texture (for SIS computation)
class ScenetexDepth : public Scenetex {
    private:
        VectorUptr<Object> _uptrObjects;
        VectorUptr<Mesh> _uptrMeshes;
        MapUptr<Texture> _uptrTextures;
        MapUptr<Material> _uptrMaterials;

    public:
        /// \brief init scene, framebuffer
        void init(int texWidth, int texHeight,
                const std::string & sisSceneFilename,
                const glm::mat4 & sisSceneMatrix);

        /// \brief render the scene in the texture
        ///
        /// \param T transform matrix applied to the scene before rendering
        void render(const glm::mat4 &T);
};

/// \brief render a texture map for SIS
///
/// can render both random dots map and image map
class ScenetexMap : public Scenetex {
    private:
        Random _rng;
        Texture _textureImage;
        Shader _shaderImage;

    public:
        /// \brief init framebuffer, image
        void init(int texWidth, int texHeight,
                const std::string & imageMapFilename);

        /// \brief render the map specified in config
        void render(const SisConfig & config);
};

/// \brief render texture map in screen-space
class ScenetexScreen : public Scenetex {
    private:
        Scenetex * _ptrScenetex;
        VectorUptr<Object> * _ptrUptrSisObjects;

    public:
        /// \brief init framebuffer, texture map
        void init(int texWidth, int texHeight,
                Scenetex * ptrScenetex,
                VectorUptr<Object> * ptrUptrSisObjects);

        /// \brief render texture map in screen-space
        void render(int frameWidth, int frameHeight, const glm::mat4 &VP,
                const SisConfig & config);
};

/// \brief implement complete SIS computation
class ScenetexSis : public Scenetex {
    private:
        ScenetexDepth _scenetexDepth;
        ScenetexMap _scenetexMap;
        ScenetexScreen _scenetexScreenDepth;
        ScenetexScreen _scenetexScreenMap;

        FrameBuffer _fbo2;
        FrameBuffer * _ptrFboSrc;
        FrameBuffer * _ptrFboDst;

    public:
        /// \brief init SIS element (texture map, SIS scene, output texture)
        void init(int texWidth, int texHeight,
                VectorUptr<Object> * ptrUptrSisObjects,
                int depthWidth, int depthHeight,
                const std::string & sisSceneFilename,
                const glm::mat4 & sisSceneMatrix,
                int mapWidth, int mapHeight,
                const std::string & imageMapFilename);

        /// \brief init SIS elements (texture map, SIS scene, output texture)
        void render(int frameWidth, int frameHeight, const glm::mat4 &T,
                const glm::mat4 &VP, const SisConfig & config);

        /// \brief bind framebuffer texture (SIS)
        void bindTextureFbo();

        /// \brief bind depth texture (SIS scene)
        void bindTextureDepth();

        /// \brief get rendered texture width
        int getFboWidth();

        /// \brief get rendered texture height
        int getFboHeight();
};

#endif
