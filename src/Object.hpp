// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include "Mesh.hpp"
#include "Material.hpp"

/// \brief Object (mesh, material, model matrix)
class Object {
    private:
        glm::mat4 _modelMatrix;
        Mesh * _ptrMesh;
        Material * _ptrMaterial;

    public:
        /// \brief main constructor
        Object(const glm::mat4 & modelMatrix, Mesh * ptrMesh,
                Material * ptrMaterial);

        /// \brief get transform matrix
        const glm::mat4 & getModelMatrix() const;

        /// \brief get mesh pointer
        const Mesh * getPtrMesh() const;

        /// \brief get material pointer
        const Material * getPtrMaterial() const;
};

#endif
