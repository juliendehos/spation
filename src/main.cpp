// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

/// \mainpage Single-Image-Stereogram embedding in 3D scene
///
/// This program displays a single-image-stereogram (SIS) into a 3D scene. Thus,
/// the final displayed image contains both the main 3D scene and the included SIS
/// scene. The viewer can see the main scene, when focusing his eyes on the display
/// screen, or the SIS scene, when focusing behind. The method is implemented on
/// the GPU and produces Full-HD images in real-time.
///
/// More details at http://www-lisic.univ-littoral.fr/~dehos/recherche/spation/
///
/// Build instructions in README

#include "FpsCounter.hpp"
#include "KeyboardMotion.hpp"
#include "MouseMotion.hpp"
#include "Controller.hpp"
#include "SisConfig.hpp"
#include "Util.hpp"

#include <iostream>
#include <sstream>

#include <GL/freeglut.h>  // for glutLeaveMainLoop
//#include <GL/glut.h>

// window parameters
bool gFullscreen = false;
bool gShowHud = true;

// translations (keyboard)
KeyboardMotion gKeyForward(4);
KeyboardMotion gKeyBackward(4);
KeyboardMotion gKeyLeft(3);
KeyboardMotion gKeyRight(3);
KeyboardMotion gKeyUp(2);
KeyboardMotion gKeyDown(2);

// orientation (mouse)
MouseMotion gMouse(0, 0, -0.002, -0.002);

Controller gController(1280, 720);
FpsCounter gFpsCounter;
float gFps = 0;
SisConfig gConfigSis;

void switchFullscreen() {
    gFullscreen = not gFullscreen;
    if (gFullscreen) glutFullScreen();
    else glutReshapeWindow(gController.getDefaultWindowWidth(), 
            gController.getDefaultWindowHeight());
}

// display text using OpenGL
void renderText(std::stringstream & oss, int x, int y, int w, int h) {
    // set projection
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, w, h, 0);

    // set modelview
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    // render bitmap text
    glRasterPos3i(x, y, 0);
    const std::string text = oss.str();
    for (const char * c=text.c_str(); *c != '\0'; c++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
    }
    oss.str( std::string() );
    oss.clear();

    // restore projection
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    // restore modelview
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

void processDisplay() {

    // render the scene
    gController.render(gConfigSis);

    if (gShowHud) {
        // print info
        glColor4d(1, 1, 1, 1);
        std::stringstream oss;
        int windowWidth = gController.getWindowWidth();
        int windowHeight = gController.getWindowHeight();
        renderText(oss, 20, 10, windowWidth, windowHeight);
        oss << "strip width (t/g) = " << gConfigSis._stripSlider.getValue();
        renderText(oss, 20, 30, windowWidth, windowHeight);
        oss << "depth factor (y/h) = " << gConfigSis._depthSlider.getValue();
        renderText(oss, 20, 50, windowWidth, windowHeight);
        oss << "texture resolution (u/j) = " 
            << gConfigSis._resolutionSlider.getValue();
        renderText(oss, 20, 70, windowWidth, windowHeight);
        oss << "SIS map (i/k) = " << gConfigSis._mapSlider.getValue();
        renderText(oss, 20, 90, windowWidth, windowHeight);
        oss << "scene (o/l) = " << gConfigSis._sceneSlider.getValue();
        renderText(oss, 20, 110, windowWidth, windowHeight);
        oss << "fullscreen (f) = " << (gFullscreen ? "on" : "off");
        renderText(oss, 20, 130, windowWidth, windowHeight);
        oss << "use arrow/page/esc keys" ;
        renderText(oss, 20, 150, windowWidth, windowHeight);

        // print FPS
        gFpsCounter.countNewFrame();
        if (gFpsCounter.hasFps()) gFps = gFpsCounter.getFpsAndRestart();
        oss << "FPS = " << (int)gFps;
        renderText(oss, 20, 170, windowWidth, windowHeight);
    }

    glutSwapBuffers();
}

void processReshape(int w, int h) {
    gController.resizeWindow(w, h);
}

void processIdle() {

    // update camera
    float tx = gKeyRight.getValueAndUpdate() - gKeyLeft.getValueAndUpdate();
    float ty = gKeyUp.getValueAndUpdate() - gKeyDown.getValueAndUpdate();
    float tz = gKeyBackward.getValueAndUpdate() 
        - gKeyForward.getValueAndUpdate();
    // mouse y controls rotation around opengl x-axis
    float rx = gMouse.getYValue();
    // mouse x controls rotation around opengl y-axis
    float ry = gMouse.getXValue();
    gController.updateView(tx, ty, tz, rx, ry, 0.f);

    glutPostRedisplay();
}

void processMotion(int x, int y) {
    gMouse.motion(x, y);
}

void processMouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) gMouse.press(x, y);
        else gMouse.release(x, y);
    }
}

void processSpecialDown(int key, int /*x*/, int /*y*/) {
    switch (key) {
        case GLUT_KEY_UP : gKeyForward.press(); break;
        case GLUT_KEY_DOWN : gKeyBackward.press(); break;
        case GLUT_KEY_PAGE_DOWN : gKeyDown.press(); break;
        case GLUT_KEY_PAGE_UP : gKeyUp.press(); break;
        case GLUT_KEY_LEFT : gKeyLeft.press(); break;
        case GLUT_KEY_RIGHT : gKeyRight.press(); break;
    }
}

void processSpecialUp(int key, int /*x*/, int /*y*/) {
    switch (key) {
        case GLUT_KEY_UP : gKeyForward.release(); break;
        case GLUT_KEY_DOWN : gKeyBackward.release(); break;
        case GLUT_KEY_PAGE_DOWN : gKeyDown.release(); break;
        case GLUT_KEY_PAGE_UP : gKeyUp.release(); break;
        case GLUT_KEY_LEFT : gKeyLeft.release(); break;
        case GLUT_KEY_RIGHT : gKeyRight.release(); break;
    }
}

void processKeyboardUp(unsigned char key, int /*x*/, int /*y*/) {
    switch (key) {
        //case 27 : exit(0); break;
        case 27 : glutLeaveMainLoop(); break;
        case 'f' : switchFullscreen(); break;
        case 'r' : gShowHud = not gShowHud; break;
        case 't' : gConfigSis._stripSlider.runUp(); break;
        case 'g' : gConfigSis._stripSlider.runDown(); break;
        case 'y' : gConfigSis._depthSlider.runUp(); break;
        case 'h' : gConfigSis._depthSlider.runDown(); break;
        case 'u' : gConfigSis._resolutionSlider.runUp(); break;
        case 'j' : gConfigSis._resolutionSlider.runDown(); break;
        case 'i' : gConfigSis._mapSlider.runUp(); break;
        case 'k' : gConfigSis._mapSlider.runDown(); break;
        case 'o' : gConfigSis._sceneSlider.runUp(); break;
        case 'l' : gConfigSis._sceneSlider.runDown(); break;
    }
}

int main(int argc, char **argv) {

    // initialize glut window
    glutInit(&argc, argv);
    glutInitWindowSize(gController.getDefaultWindowWidth(), 
            gController.getDefaultWindowHeight());
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutCreateWindow("Spation");

    // initialize callback functions
    glutDisplayFunc(processDisplay);
    glutReshapeFunc(processReshape);
    glutIdleFunc(processIdle);
    glutMouseFunc(processMouse);
    glutMotionFunc(processMotion);
    glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
    glutKeyboardUpFunc(processKeyboardUp);
    glutSpecialFunc(processSpecialDown);
    glutSpecialUpFunc(processSpecialUp);

    // initialize glew
    GLenum err = glewInit();
    std::stringstream oss;
    if (GLEW_OK != err) {
        oss << "GLEW : " << glewGetErrorString(err);
        utilError(oss.str().c_str() , __FILE__, __LINE__);
    }
    else {
        oss << "GLEW : " << glewGetString(GLEW_VERSION);
        utilInfo(oss.str().c_str() , __FILE__, __LINE__);
    }

    // initialize OpenGL
    glEnable(GL_DEPTH_TEST);

    // initialize scene data
    gController.init();
    gFpsCounter.start();

    // launch main loop
    glutMainLoop();

    return 0;
}

