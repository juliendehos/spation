// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "MouseMotion.hpp"

MouseMotion::MouseMotion(float xInitialValue, float yInitialValue, 
        float xSpeed, float ySpeed) :
    _xValue(xInitialValue), _yValue(yInitialValue), _xSpeed(xSpeed), 
    _ySpeed(ySpeed), _isMotion(false) 
{
}

void MouseMotion::press(int x, int y) {
    _x0 = x;
    _y0 = y;
    _isMotion = true;
}

void MouseMotion::motion(int x, int y) {
    if (_isMotion) {
        _xValue += (x - _x0)*_xSpeed;
        _yValue += (y - _y0)*_ySpeed;
        _x0 = x;
        _y0 = y;
    }
}

void MouseMotion::release(int x, int y) {
    motion(x, y);
    _isMotion = false;
}

float MouseMotion::getXValue() const {
    return _xValue;
}

float MouseMotion::getYValue() const {
    return _yValue;
}
