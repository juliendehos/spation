// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "LoaderOBJ.hpp"
#include "Scene.hpp"

// for glm::axisAngle in SceneSibenik::render
#include <glm/gtx/matrix_interpolation.hpp>

//////////////////////////////////////////////////////////////////////
// SceneTv
//////////////////////////////////////////////////////////////////////

void SceneTv::init() {

    _sceneShader.initFromFile("vsScene.glsl", "fsScene.glsl");

    LoaderOBJ loader;
    loader.loadOBJ("scene_tv.obj", glm::mat4(1.f),
            _uptrObjects, _uptrMeshes, _uptrTextures, _uptrMaterials);
    loader.loadOBJ("scene_tv_sisobject.obj", glm::mat4(1.f),
            _uptrSisObjects, _uptrMeshes, _uptrTextures, _uptrMaterials);

    glm::mat4 T = glm::translate(glm::mat4(1.f), glm::vec3(1.f, -1.5f, -3.f));
    _scenetexSis.init(2048, 2048, &_uptrSisObjects,
            1024, 1024, "sis_armadillo.obj", T,
            1024, 1024,"texture_walls.png");
}

void SceneTv::render(int width, int height, const glm::mat4 &VP,
        const SisConfig & config, const Camera & /*camera*/) {

    // render sis texture
    _scenetexSis.render(width, height, glm::mat4(1.f), VP, config);

    // final render
    glClearColor(0.0, 0.4, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, width, height);
    _sceneShader.bind();
    _sceneShader.uniform3f("_lightColor", 20.f, 20.f, 20.f);
    _sceneShader.uniform3f("_lightPosition_worldspace", 0.f, 1.f, -14.f);
    _sceneShader.uniformMatrix4fv("_VP", &VP[0][0]);

    // render main scene
    for (auto & ptrObject : _uptrObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0], ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        // bind texture
        if (ptrMaterial->_ptrTexture) {
            ptrMaterial->_ptrTexture->bind();
            _sceneShader.uniform1i("_useTexture", 1);
            _sceneShader.uniform1i("_useUv", 1);
        }
        else {
            _sceneShader.uniform1i("_useTexture", 0);
        }

        // draw mesh
        ptrObject->getPtrMesh()->draw();
        Texture::unbind();
    }


    // render sis objects
    _sceneShader.uniform1i("_useTexture", 1);
    _sceneShader.uniform1i("_useUv", 0);
    _sceneShader.uniform2f("_textureSize",
            _scenetexSis.getFboWidth(), _scenetexSis.getFboHeight());
    if (config._mapSlider.getValue() == 3) _scenetexSis.bindTextureDepth();
    else _scenetexSis.bindTextureFbo();

    for (auto & ptrObject : _uptrSisObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0],ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        ptrObject->getPtrMesh()->draw();
    }
    Texture::unbind();

    _sceneShader.unbind();	
}


//////////////////////////////////////////////////////////////////////
// SceneBarrel
//////////////////////////////////////////////////////////////////////

void SceneBarrel::init() {

    _sceneShader.initFromFile("vsScene.glsl", "fsScene.glsl");

    glm::mat4 T0 = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.4f, 6.f));
    LoaderOBJ loader;
    loader.loadOBJ("scene_barrel.obj", T0, _uptrObjects, _uptrMeshes,
            _uptrTextures, _uptrMaterials);
    loader.loadOBJ("scene_barrel_sisobject.obj", T0, _uptrSisObjects,
            _uptrMeshes, _uptrTextures, _uptrMaterials);

    glm::mat4 T = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -1.5f, -1.f));
    _scenetexSis.init(2048, 2048, &_uptrSisObjects,
            1024, 1024, "sis_trou.obj", T,
            1024, 1024,"texture_water.png");
}

void SceneBarrel::render(int width, int height, const glm::mat4 &VP,
        const SisConfig & config, const Camera & /*camera*/) {

    // render sis texture
    _scenetexSis.render(width, height, glm::mat4(1.f), VP, config);

    // final render
    glClearColor(0.4, 0.6, 0.8, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, width, height);
    _sceneShader.bind();
    _sceneShader.uniform3f("_lightColor", 20.f, 20.f, 20.f);
    _sceneShader.uniform3f("_lightPosition_worldspace", 0.f, 2.f, -8.f);
    _sceneShader.uniformMatrix4fv("_VP", &VP[0][0]);

    // render main scene
    for (auto & ptrObject : _uptrObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0], ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        // bind texture
        if (ptrMaterial->_ptrTexture) {
            ptrMaterial->_ptrTexture->bind();
            _sceneShader.uniform1i("_useTexture", 1);
            _sceneShader.uniform1i("_useUv", 1);
        }
        else {
            _sceneShader.uniform1i("_useTexture", 0);
        }

        // draw mesh
        ptrObject->getPtrMesh()->draw();
        Texture::unbind();
    }


    // render sis objects
    _sceneShader.uniform1i("_useTexture", 1);
    _sceneShader.uniform1i("_useUv", 0);
    _sceneShader.uniform2f("_textureSize",
            _scenetexSis.getFboWidth(), _scenetexSis.getFboHeight());
    if (config._mapSlider.getValue() == 3) _scenetexSis.bindTextureDepth();
    else _scenetexSis.bindTextureFbo();

    for (auto & ptrObject : _uptrSisObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0], ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        ptrObject->getPtrMesh()->draw();
    }
    Texture::unbind();

    _sceneShader.unbind();
}

//////////////////////////////////////////////////////////////////////
// SceneDragons
//////////////////////////////////////////////////////////////////////

void SceneDragons::init() {

    _sceneShader.initFromFile("vsScene.glsl", "fsScene.glsl");

    LoaderOBJ loader;
    loader.loadOBJ("scene_dragons.obj", glm::mat4(1.f),
            _uptrObjects, _uptrMeshes, _uptrTextures, _uptrMaterials);
    loader.loadOBJ("scene_dragons_sisobject.obj", glm::mat4(1.f),
            _uptrSisObjects, _uptrMeshes, _uptrTextures, _uptrMaterials);

    glm::mat4 T = glm::translate(glm::mat4(1.f), glm::vec3(1.f, -1.5f, -2.f));
    _scenetexSis.init(2048, 2048, &_uptrSisObjects,
            1024, 1024, "sis_dragon.obj", T,
            1024, 1024,"texture_walls.png");
}

void SceneDragons::render(int width, int height, const glm::mat4 &VP,
        const SisConfig & config, const Camera & /*camera*/) {

    // render sis texture
    _scenetexSis.render(width, height, glm::mat4(1.f), VP, config);


    // final render
    glClearColor(0.0, 0.4, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, width, height);
    _sceneShader.bind();
    _sceneShader.uniform3f("_lightColor", 20.f, 20.f, 20.f);
    _sceneShader.uniform3f("_lightPosition_worldspace", 0.f, 1.f, -14.f);
    _sceneShader.uniformMatrix4fv("_VP", &VP[0][0]);


    // render main scene
    for (auto & ptrObject : _uptrObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0],ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        // bind texture
        if (ptrMaterial->_ptrTexture) {
            ptrMaterial->_ptrTexture->bind();
            _sceneShader.uniform1i("_useTexture", 1);
            _sceneShader.uniform1i("_useUv", 1);
        }
        else {
            _sceneShader.uniform1i("_useTexture", 0);
        }

        // draw mesh
        ptrObject->getPtrMesh()->draw();
        Texture::unbind();
    }


    // render sis objects
    _sceneShader.uniform1i("_useTexture", 1);
    _sceneShader.uniform1i("_useUv", 0);
    _sceneShader.uniform2f("_textureSize",
            _scenetexSis.getFboWidth(), _scenetexSis.getFboHeight());
    if (config._mapSlider.getValue() == 3) _scenetexSis.bindTextureDepth();
    else _scenetexSis.bindTextureFbo();

    for (auto & ptrObject : _uptrSisObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0], ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        ptrObject->getPtrMesh()->draw();
    }
    Texture::unbind();

    _sceneShader.unbind();
}


//////////////////////////////////////////////////////////////////////
// SceneSibenik
//////////////////////////////////////////////////////////////////////

void SceneSibenik::init() {

    _sceneShader.initFromFile("vsScene.glsl", "fsScene.glsl");

    LoaderOBJ loader;
    loader.loadOBJ("scene_sibenik.obj", glm::mat4(1.f),
            _uptrObjects, _uptrMeshes, _uptrTextures, _uptrMaterials);
    loader.loadOBJ("scene_sibenik_sisobject.obj", glm::mat4(1.f),
            _uptrSisObjects, _uptrMeshes, _uptrTextures, _uptrMaterials);

    glm::mat4 T = glm::translate(glm::mat4(1.f), glm::vec3(0.f, -4.f, -15.f));
    _scenetexSis.init(2048, 2048, &_uptrSisObjects,
            1024, 1024, "sis_armadillo.obj", T,
            1024, 1024,"texture_marble.png");

    _pathLight.addKey(-1, glm::vec3(-2,  2, -18));
    _pathLight.addKey( 0, glm::vec3( 2,  2, -16));
    _pathLight.addKey( 1, glm::vec3( 2,  2, -18));
    _pathLight.addKey( 2, glm::vec3(-2,  2, -16));
    // repeat the first 3 keys
    _pathLight.addKey( 3, glm::vec3(-2,  2, -18));
    _pathLight.addKey( 4, glm::vec3( 2,  2, -16));
    _pathLight.addKey( 5, glm::vec3( 2,  2, -18));
    _chrono.start();
}

void SceneSibenik::render(int width, int height, const glm::mat4 &VP,
        const SisConfig & config, const Camera & camera) {

    glm::vec3 center = glm::vec3(0.f, -4.f, -15.f);
    glm::vec3 eye = camera.getTranslationVector();
    // no interaction
    //glm::vec3 eye = glm::vec3(0.f, -2.f, -12.f); 
    // SIS scene / 3D scene interaction
    glm::vec3 up = glm::vec3(0.f, 0.f, -1.f);
    glm::mat4 T = glm::lookAt(eye, center, up);

    // render sis texture
    _scenetexSis.render(width, height, T, VP, config);

    // animation
    double time = _chrono.elapsed();
    double duration = _pathLight.getEndTime(); // assumes getStartTime() == 0
    int repetitions = int(time/duration);
    time -= repetitions*duration;
    glm::vec3 lightPosition = _pathLight.getValue(time);

    // final render
    glClearColor(0.0, 0.4, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, width, height);
    _sceneShader.bind();
    _sceneShader.uniform3f("_lightPosition_worldspace",
            lightPosition[0], lightPosition[1], lightPosition[2]);
    _sceneShader.uniform3f("_lightColor", 20.f, 20.f, 20.f);
    _sceneShader.uniformMatrix4fv("_VP", &VP[0][0]);


    // render main scene
    for (auto & ptrObject : _uptrObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0], ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        // bind texture
        if (ptrMaterial->_ptrTexture) {
            ptrMaterial->_ptrTexture->bind();
            _sceneShader.uniform1i("_useTexture", 1);
            _sceneShader.uniform1i("_useUv", 1);
        }
        else {
            _sceneShader.uniform1i("_useTexture", 0);
        }

        // draw mesh
        ptrObject->getPtrMesh()->draw();
        Texture::unbind();
    }

    // render sis objects
    _sceneShader.uniform1i("_useTexture", 1);
    _sceneShader.uniform1i("_useUv", 0);
    _sceneShader.uniform2f("_textureSize",
            _scenetexSis.getFboWidth(), _scenetexSis.getFboHeight());
    if (config._mapSlider.getValue() == 3) _scenetexSis.bindTextureDepth();
    else _scenetexSis.bindTextureFbo();

    for (auto & ptrObject : _uptrSisObjects) {

        // load model matrix
        glm::mat4 M = ptrObject->getModelMatrix();
        _sceneShader.uniformMatrix4fv("_M", &M[0][0]);

        // load material
        const Material * ptrMaterial = ptrObject->getPtrMaterial();
        _sceneShader.uniform3f("_ambientColor",
                ptrMaterial->_ka[0], ptrMaterial->_ka[1], ptrMaterial->_ka[2]);
        _sceneShader.uniform3f("_diffuseColor",
                ptrMaterial->_kd[0], ptrMaterial->_kd[1], ptrMaterial->_kd[2]);

        ptrObject->getPtrMesh()->draw();
    }
    Texture::unbind();

    _sceneShader.unbind();
}

