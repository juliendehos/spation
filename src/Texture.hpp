// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef TEXTURE_HPP_
#define TEXTURE_HPP_

#include "Ogl.hpp"

#include <string>

/// \brief OpenGL Texture loaded from a png file
class Texture {
    private:
        GLuint _textureId;

    public:
        /// \brief load a texture from a png file
        ///
        /// Call this method before bind/unbind.
        /// Png file only.
        bool initFromFile(const std::string & filename);

        /// \brief bind texture in GPU
        void bind() const;

        /// \brief unbind texture in GPU
        static void unbind();
};

#endif
