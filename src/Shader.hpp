// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef SHADER_HPP_
#define SHADER_HPP_

#include "Ogl.hpp"

#include <string>

/// \brief OpenGL Shader
class Shader {
    private:
        GLuint _programId;

    public:

        /// \brief initialize a shader program from a string
        bool initFromString(const std::string & vertexShaderSource,
                const std::string & fragmentShaderSource);

        /// \brief initialize a shader program from a text file
        void initFromFile(const std::string & vsFilename,
                const std::string & fsFilename);

        /// \brief bind shader program in GPU
        void bind() const;

        /// \brief unbind shader program in GPU
        static void unbind();

        /// \brief Specify the value of a uniform variable (int)
        ///
        /// \pre Bind shader first
        void uniform1i(const char * location, GLint i1) const;

        /// \brief Specify the value of a uniform variable (float)
        ///
        /// \pre Bind shader first
        void uniform1f(const char * location, GLfloat v1) const;

        /// \brief Specify the value of a uniform variable (ivec2)
        ///
        /// \pre Bind shader first
        void uniform2i(const char * location, GLint i1, GLint i2) const;

        /// \brief Specify the value of a uniform variable (vec2)
        ///
        /// \pre Bind shader first
        void uniform2f(const char * location, GLfloat v1, GLfloat v2) const;

        /// \brief Specify the value of a uniform variable (vec3)
        ///
        /// \pre Bind shader first
        void uniform3f(const char * location, GLfloat v1, GLfloat v2,
                GLfloat v3) const;

        /// \brief Specify the value of a uniform variable (vec4)
        ///
        /// \pre Bind shader first
        void uniform4f(const char * location, GLfloat v1, GLfloat v2,
                GLfloat v3, GLfloat v4) const;

        /// \brief Specify the value of a uniform variable (mat4)
        ///
        /// \pre Bind shader first
        void uniformMatrix4fv(const char * location,  const GLfloat *value) const;

    private:
        GLuint loadShaderFromString(GLenum type, const std::string & source) const;
        std::string readTextFile(const std::string & filename) const;
};

#endif
