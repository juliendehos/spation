// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Controller.hpp"
#include "Ogl.hpp"

#include <iostream>

void Controller::init() {

    _scenes.push_back(std::unique_ptr<Scene>(new SceneTv));
    _scenes.push_back(std::unique_ptr<Scene>(new SceneBarrel));
    _scenes.push_back(std::unique_ptr<Scene>(new SceneSibenik));
    _scenes.push_back(std::unique_ptr<Scene>(new SceneDragons));

    for (auto & uptrScene : _scenes) uptrScene->init();
}

int Controller::getDefaultWindowWidth() const {
    return _defaultWindowWidth;
}

int Controller::getDefaultWindowHeight() const {
    return _defaultWindowHeight;
}

int Controller::getWindowWidth() const {
    return _windowWidth;
}

int Controller::getWindowHeight() const {
    return _windowHeight;
}

void Controller::render(const SisConfig & config) {

    // projection-view matrix
    glm::mat4 V = _camera.getOrientationMatrix()
        * _camera.getTranslationMatrix();
    glm::mat4 P = glm::perspective(45.f,
            _windowWidth/float(_windowHeight), 0.2f, 50.f);
    glm::mat4 VP = P*V;

    int nbScenes = _scenes.size();
    assert(nbScenes > 0);
    int iScene = config._sceneSlider.getValue();
    iScene = std::max(iScene, 0);
    iScene = std::min(iScene, nbScenes-1);
    Scene * ptrScene = _scenes[iScene].get();

    ptrScene->render(_windowWidth, _windowHeight, VP, config, _camera);
}

Controller::Controller(int defaultWindowWidth, int defaultWindowHeight) :
    _defaultWindowWidth(defaultWindowWidth),
    _defaultWindowHeight(defaultWindowHeight) {
    }

void Controller::resizeWindow(int width, int height) {
    _windowWidth = width;
    _windowHeight = std::max(2, height);
}

void Controller::updateView(float tx, float ty, float tz,
        float rx, float ry, float rz) {
    _camera.updateView(tx, ty, tz, rx, ry, rz);
}

