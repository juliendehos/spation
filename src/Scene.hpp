// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef SCENE_HPP_
#define SCENE_HPP_

#include "Camera.hpp"
#include "Chrono.hpp"
#include "SisConfig.hpp"
#include "Scenetex.hpp"
#include "Spline.hpp"
#include "Util.hpp"

/// \brief 3D scene + SIS
class Scene {
    protected:
        ScenetexSis _scenetexSis;

        Shader _sceneShader;

        VectorUptr<Object> _uptrObjects;
        VectorUptr<Object> _uptrSisObjects;

        VectorUptr<Mesh> _uptrMeshes;
        MapUptr<Texture> _uptrTextures;
        MapUptr<Material> _uptrMaterials;

    public:
        virtual void init() = 0;
        virtual void render(int width, int height, const glm::mat4 &VP, 
                const SisConfig & config, const Camera & camera) = 0;
};

/// \brief SIS on TV screen
class SceneTv : public Scene {
    public :
        void init();
        void render(int width, int height, const glm::mat4 &VP, 
                const SisConfig & config, const Camera & camera);
};

/// \brief SIS in barrel
class SceneBarrel : public Scene {
    public :
        void init();
        void render(int width, int height, const glm::mat4 &VP, 
                const SisConfig & config, const Camera & camera);
};

/// \brief SIS over one wall
class SceneDragons : public Scene {
    public :
        void init();
        void render(int width, int height, const glm::mat4 &VP, 
                const SisConfig & config, const Camera & camera);
};

/// \brief SIS in sibenik (SIS dynamically oriented toward viewpoint)
class SceneSibenik : public Scene {
    private:
        Chrono _chrono;
        Spline3<glm::vec3> _pathLight;
    public :
        void init();
        void render(int width, int height, const glm::mat4 &VP, 
                const SisConfig & config, const Camera & camera);
};

#endif
