// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef SISCONFIG_HPP_
#define SISCONFIG_HPP_

#include <string>

/// \brief configuration parameters for SIS computation
class SisConfig {
public:
	class Slider {
        private:
            std::string _name;
            int _currentValue;
            int _minValue;
            int _maxValue;
            int _increment;
        public:
            Slider(const std::string &name, int currentValue, int minValue, 
                    int maxValue, int increment);
            void runUp();
            void runDown();
            int getValue() const;
    };

    Slider _mapSlider;
    Slider _sceneSlider;
    Slider _resolutionSlider;
    Slider _depthSlider;
    Slider _stripSlider;

    SisConfig();
};

#endif

