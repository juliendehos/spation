// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "KeyboardMotion.hpp"

KeyboardMotion::KeyboardMotion(float speed) :
    _value(0), _speed(speed), _isMotion(false) 
{
}

void KeyboardMotion::press() {
    _value = 0;
    _t0 = std::chrono::system_clock::now();
    _isMotion = true;
}

void KeyboardMotion::update() {
    if (_isMotion) {
        std::chrono::time_point<std::chrono::system_clock> t1 =
            std::chrono::system_clock::now();
        float deltaT = 1e-3 * std::chrono::duration_cast
            < std::chrono::milliseconds > (t1 - _t0).count();
        _value += deltaT*_speed;
        _t0 = t1;
    }
}

void KeyboardMotion::release() {
    update();
    _isMotion = false;
}

float KeyboardMotion::getValueAndUpdate() {
    update();
    float value = _isMotion ? _value : 0;
    _value = 0;
    return value;
}
