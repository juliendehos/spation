// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "SisConfig.hpp"

#include <iostream>

SisConfig::Slider::Slider(const std::string &name, int currentValue,
        int minValue, int maxValue, int increment) :
    _name(name),
    _currentValue(currentValue), 
    _minValue(minValue), 
    _maxValue(maxValue), 
    _increment(increment) 
{
}

void SisConfig::Slider::runUp() {
    _currentValue = std::min(_maxValue, _currentValue+_increment);
}

void SisConfig::Slider::runDown() {
    _currentValue = std::max(_minValue, _currentValue-_increment);
}

int SisConfig::Slider::getValue() const {
    return _currentValue;	
}

SisConfig::SisConfig() :
    _mapSlider("texture map", 0, 0, 3, 1),
    _sceneSlider("scene", 0, 0, 3, 1),
    _resolutionSlider("texture resolution", 200, 50, 500, 50),
    _depthSlider("depth factor", 50, 10, 100, 10),
    _stripSlider("strip width", 150, 100, 300, 10) 
{
}
