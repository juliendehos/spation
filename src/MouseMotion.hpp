// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef MOUSEMOTION_HPP_
#define MOUSEMOTION_HPP_

/// \brief control two variables with mouse
class MouseMotion {
    private:
        float _xValue;
        float _yValue;
        float _xSpeed;
        float _ySpeed;
        float _x0;
        float _y0;
        bool _isMotion;

    public:
        /// \brief constructor
        MouseMotion(float xInitialValue, float yInitialValue,
                float xSpeed, float ySpeed);

        /// \brief press button : start updating values
        void press(int x, int y);

        /// \brief move mouse : update values
        void motion(int x, int y);

        /// \brief release button : stop updating values
        void release(int x, int y);

        /// \brief get X value (ABSOLUTE value since last call to press)
        float getXValue() const;

        /// \brief get Y value (ABSOLUTE value since last call to press)
        float getYValue() const;
};

#endif
