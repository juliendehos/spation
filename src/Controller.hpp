// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef CONTROLLER_HPP_
#define CONTROLLER_HPP_

#include "Camera.hpp"
#include "SisConfig.hpp"
#include "Scene.hpp"

#include <memory>
#include <vector>

/// \brief MVC controller
class Controller {

    private:
        std::vector< std::unique_ptr< Scene > > _scenes;

        Camera _camera;

        int _defaultWindowWidth;
        int _defaultWindowHeight;

        int _windowHeight;
        int _windowWidth;

    public:
        /// \brief main constructor
        Controller(int defaultWindowWidth, int defaultWindowHeight);

        /// \brief initialize scenes
        ///
        /// Call this method first.
        void init();

        /// \brief render the scene specified in config
        void render(const SisConfig & config);

        /// \brief get default width (defined with init)
        int getDefaultWindowWidth() const;

        /// \brief get default height (defined with init)
        int getDefaultWindowHeight() const;

        /// \brief get current width
        int getWindowWidth() const;

        /// \brief get current height
        int getWindowHeight() const;

        /// \brief update window size
        void resizeWindow(int width, int height);

        /// \brief render the scene specified in config
        void updateView(float tx, float ty, float tz,
                float rx, float ry, float rz);
};

#endif
