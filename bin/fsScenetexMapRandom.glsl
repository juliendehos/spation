#version 330 core

layout(location = 0) out vec4 _fragmentColor;

uniform	float _time;
uniform	int _resolution;

in vec2 _uv;

//////////////////////////////////////////////////////////////////////////////
// random 
//////////////////////////////////////////////////////////////////////////////

// A single iteration of Bob Jenkins' One-At-A-Time hashing algorithm.
uint hash1( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

// Compound versions of the hashing algorithm I whipped together.
uint hash3( uvec3 v ) {
	return hash1( v.x ^ hash1(v.y) ^ hash1(v.z));
}

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}

// Pseudo-random value in half-open range [0:1].
float random2t(vec2 xy, float t) {
	vec3 P = vec3(xy.x, xy.y, t);
	return floatConstruct(hash3(floatBitsToUint(P)));
}

//////////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////////

void main() {
	
	vec2 xy = ivec2(_uv * _resolution) / float(_resolution);
	
	float rand_1 = random2t(xy, _time);
	//float rand_2 = random2t(xy, _time+1);
	//float rand_3 = random2t(xy, _time+2);
	//_fragmentColor = vec4(rand_1, rand_2, rand_3, 1.f);
	_fragmentColor = vec4(rand_1, rand_1, rand_1, 1.f);
}


