#version 330 core

layout(location = 0) out vec4 _fragmentColor;

uniform sampler2D _texture;

in vec2 _uv;

void main() {
	_fragmentColor = texture( _texture, _uv);
}
