#version 330 core

layout(location = 0) out vec4 _fragmentColor;

void main() {
	float depth = 0.06*gl_FragCoord.z/gl_FragCoord.w;
	_fragmentColor = vec4(depth, depth, depth, 1.f);
}
