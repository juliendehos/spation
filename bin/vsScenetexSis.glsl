#version 330 core

layout(location = 0) in vec3 _vertexPosition_modelspace;
layout(location = 1) in vec3 _vertexNormal_modelspace;
layout(location = 2) in vec2 _vertexUv_modelspace;

out vec2 _uv; // in [0,1]x[0,1]

void main() {
	_uv = 0.5 * ( vec2(1, 1)+_vertexPosition_modelspace.xy );
    gl_Position = vec4(_vertexPosition_modelspace, 1.f); 
}
