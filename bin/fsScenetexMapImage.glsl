#version 330 core

layout(location = 0) out vec4 _fragmentColor;

uniform	float _repetitions;

in vec2 _uv;

uniform sampler2D _texture;

void main() {
	
	vec2  uv0 = _uv * _repetitions;
	ivec2 uv1 = ivec2(uv0);
	vec2  uv2 = uv0 - uv1;
	
	_fragmentColor = texture(_texture, uv2);
}


