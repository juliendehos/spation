#version 330 core

layout(location = 0) in vec3 _vertexPosition_modelspace;
layout(location = 1) in vec3 _vertexNormal_modelspace;
layout(location = 2) in vec2 _vertexUv_modelspace;

uniform mat4 _MVP;

void main() {
	gl_Position = _MVP * vec4(_vertexPosition_modelspace, 1.f); 
}
