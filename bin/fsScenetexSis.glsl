#version 330 core

layout(location = 0) out vec4 _fragmentColor;

uniform sampler2D _textureScenetex;
uniform sampler2D _textureDepthScreen;
uniform sampler2D _textureMapScreen;

uniform int _x0;
uniform int _x1;
uniform int _stripWidth;
uniform int _depthFactor;

uniform vec2 _frameSize;
uniform vec2 _textureSize;

in vec2 _uv; // in [0,1]x[0,1]

vec2 xyToSt(vec2 xy) { return xy / _textureSize; }
vec2 stToXy(vec2 st) { return st * _textureSize; }
vec2 uvToXy(vec2 uv) { return uv * _frameSize; }
vec2 uvToSt(vec2 uv) { return uv * _frameSize / _textureSize; }

void main() {

	// if not current strip, copy _textureScenetex
	vec2 xy = uvToXy(_uv);
	int x = int(xy.x);
	int y = int(xy.y);
	vec2 st = uvToSt(_uv);
	if ( x<_x0 || x>_x1 ) {
		_fragmentColor = texture(_textureScenetex, st);
		return;
	}
	
	// if no SIS object, output transparent fragment
	vec4 depthColor = texture(_textureDepthScreen, st);
	if ( depthColor.a==0 ) {
		_fragmentColor = vec4(0);
		return;
	}
	
	// if invalid _stripWidth or _depthFactor, copy _textureMapScreen
	if ( _stripWidth <= _depthFactor) {
		_fragmentColor = texture(_textureMapScreen, st);		
		return;
	}
		
	// if other strips, copy _textureScenetex if not transparent or _textureMapScreen otherwise
	float depth = 1 - depthColor.r;		
	int xp = x + int(_depthFactor*depth) - _stripWidth;
	vec2 xyp = vec2(xp, y);
	vec2 stp = xyToSt(xyp);
	vec4 scenetexColor = texture(_textureScenetex, stp);
	if ( scenetexColor.a > 0 && x>=_stripWidth ) {
		_fragmentColor = scenetexColor;
	}
	else {
		_fragmentColor = texture(_textureMapScreen, st);
	}
}

